package pt.findmore.gwt.client;

/**
 * Interface to represent the messages contained in resource bundle:
 * 	/Users/mribeiro/Findmore/curso_gwt/maven/gwt261/course/src/main/resources/pt/findmore/gwt/client/Messages.properties'.
 */
public interface Messages extends com.google.gwt.i18n.client.Messages {
  
  /**
   * Translated "Enter city name".
   * 
   * @return translated "Enter city name"
   */
  @DefaultMessage("Enter city name")
  @Key("nameField")
  String nameField();

  /**
   * Translated "Send".
   * 
   * @return translated "Send"
   */
  @DefaultMessage("Send")
  @Key("sendButton")
  String sendButton();
}
