/**
 * $$Id$$
 * @author mribeiro
 * @date // :
 *
 * Copyright (C)  BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.server;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import pt.findmore.gwt.client.service.CurrentWeatherServiceException;
import pt.findmore.gwt.client.service.CurrentWeatherServiceProxy;
import pt.findmore.gwt.shared.dto.CurrentWeather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 */
public class CurrentWeatherServiceImpl extends RemoteServiceServlet
        implements CurrentWeatherServiceProxy {
    private static final long serialVersionUID = -1L;

    private static final String JSON_UTF8 = "application/json; charset=utf-8";

    public CurrentWeatherServiceImpl() { // must have
    }

    public CurrentWeather getWeatherByCity(String city) {
        try {
            String result = invokeGetRESTfulWebService("http://api.openweathermap.org/data/2.5/weather?q=" + city);
            Gson gson = new Gson();
            return gson.fromJson(result, CurrentWeather.class);
        } catch (CurrentWeatherServiceException e) {
            throw new RuntimeException("Error invoking rest api");
        } catch (JsonSyntaxException e){
            throw new RuntimeException("Error parsing rest api response");
        }

    }

    public String invokeGetRESTfulWebService(String uri)
            throws CurrentWeatherServiceException {
        try {
            URL u = new URL(uri);
            HttpURLConnection uc = (HttpURLConnection) u.openConnection();
            uc.setRequestProperty("Content-Type", JSON_UTF8);
            uc.setRequestMethod("GET");
            uc.setDoOutput(false);
            int status = uc.getResponseCode();
            if (status != 200)
                throw (new CurrentWeatherServiceException("Invalid HTTP response status code " + status + " from web service server."));
            InputStream in = uc.getInputStream();
            BufferedReader d = new BufferedReader(new InputStreamReader(in));
            String buffer = d.readLine();
            return buffer;
        } catch (MalformedURLException e) {
            throw new CurrentWeatherServiceException(e.getMessage(), e);
        } catch (IOException e) {
            throw new CurrentWeatherServiceException(e.getMessage(), e);
        }
    }
}