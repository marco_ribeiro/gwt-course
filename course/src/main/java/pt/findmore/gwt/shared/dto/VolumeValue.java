/**
 * $$Id$$
 * @author mribeiro
 * @date 23/02/15 21:58
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 */
public class VolumeValue implements IsSerializable {

    private double last3h;

    public void set3h(double value){
        this.last3h = value;
    }

    public double get3h(){
        return last3h;
    }
}