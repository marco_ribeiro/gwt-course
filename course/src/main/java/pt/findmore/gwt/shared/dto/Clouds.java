/**
 * $$Id$$
 * @author mribeiro
 * @date 23/02/15 21:58
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 */
public class Clouds implements IsSerializable {

    private double all;

    public double getAll() {
        return all;
    }

    public void setAll(double all) {
        this.all = all;
    }
}