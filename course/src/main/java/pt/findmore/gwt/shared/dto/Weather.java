/**
 * $$Id$$
 * @author mribeiro
 * @date 18/02/15 23:42
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Weather implements IsSerializable {

    private final static String ICON_BASE_URI= "http://openweathermap.org/img/w/";
    private final static String ICON_EXTENSION = ".png";

    private String main;
    private String description;
    private String icon;

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIconUri(){
        return ICON_BASE_URI + icon + ICON_EXTENSION;
    }
}