/**
 * $$Id$$
 * @author mribeiro
 * @date 23/02/15 22:12
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.Date;

/**
 *
 */
public class System implements IsSerializable {

    private String country;
    private long sunrise;
    private long sunset;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getSunrise() {
        return new Date(sunrise);
    }

    public void setSunrise(long sunrise) {
        this.sunrise = sunrise;
    }

    public Date getSunset() {
        return new Date(sunset);
    }

    public void setSunset(long sunset) {
        this.sunset = sunset;
    }
}