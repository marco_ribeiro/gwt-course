/**
 * $$Id$$
 * @author mribeiro
 * @date 04/03/15 23:38
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.shared;

import com.google.gwt.i18n.client.NumberFormat;

/**
 *
 */
public class Utils {

    private final static double TO_CELSIUS = 273.15;

    public static String formatPercentage(double value){
        return NumberFormat.getPercentFormat().format(value);
    }
    public static String formatTemperatureInCelsius(double value){
        return NumberFormat.getFormat("#,##0ºC").format((value-TO_CELSIUS));
    }
}