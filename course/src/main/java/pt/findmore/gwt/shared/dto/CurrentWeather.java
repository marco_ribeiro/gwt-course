/**
 * $$Id$$
 * @author mribeiro
 * @date 23/02/15 22:07
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.Date;

/**
 *
 */
public class CurrentWeather implements IsSerializable{

    private final static String getUri = "http://api.openweathermap.org/data/2.5/weather?q=";

    private String id;
    private long dt;
    private String name;
    private Weather[] weather;
    private Coordinate coord;
    private MainWeather main;
    private Wind wind;
    private Clouds clouds;
    private Snow snow;
    private System sys;
    private Rain rain;

    public Weather[] getWeather() {
        return weather;
    }

    public void setWeather(Weather[] weather) {
        this.weather = weather;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDt() {
        return new Date(dt);
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coordinate getCoord() {
        return coord;
    }

    public void setCoord(Coordinate coord) {
        this.coord = coord;
    }

    public MainWeather getMain() {
        return main;
    }

    public void setMain(MainWeather main) {
        this.main = main;
    }

    public static String getGetUri(){
        return getUri;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Snow getSnow() {
        return snow;
    }

    public void setSnow(Snow snow) {
        this.snow = snow;
    }

    public System getSys() {
        return sys;
    }

    public void setSys(System sys) {
        this.sys = sys;
    }

    public Rain getRain() {
        return rain;
    }

    public void setRain(Rain rain) {
        this.rain = rain;
    }
}