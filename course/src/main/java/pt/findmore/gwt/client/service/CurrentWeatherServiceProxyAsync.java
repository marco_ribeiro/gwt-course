package pt.findmore.gwt.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import pt.findmore.gwt.shared.dto.CurrentWeather;
import pt.findmore.gwt.shared.dto.Weather;

public interface CurrentWeatherServiceProxyAsync {
    void invokeGetRESTfulWebService(String uri, AsyncCallback<String> async);

    void getWeatherByCity(String city, AsyncCallback<CurrentWeather> async);
}
