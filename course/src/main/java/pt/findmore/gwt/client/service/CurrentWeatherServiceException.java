/**
 * $$Id$$
 * @author mribeiro
 * @date 13/02/15 22:20
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.client.service;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 */
public class CurrentWeatherServiceException extends Exception implements IsSerializable {
    private static final long serialVersionUID = 1L;
    private String message;

    public CurrentWeatherServiceException() {
    }

    public CurrentWeatherServiceException(String message) {
        super(message);
        this.message = message;
    }

    public CurrentWeatherServiceException(Throwable cause) {
        super(cause);
    }

    public CurrentWeatherServiceException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}