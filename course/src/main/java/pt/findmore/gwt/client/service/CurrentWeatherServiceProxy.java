/**
 * $$Id$$
 * @author mribeiro
 * @date 13/02/15 22:17
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.client.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import pt.findmore.gwt.shared.dto.CurrentWeather;
import pt.findmore.gwt.shared.dto.Weather;

/**
 *
 */
@RemoteServiceRelativePath("CurrentWeatherServiceProxy")
public interface CurrentWeatherServiceProxy extends RemoteService {
    public static class Util {
        public static CurrentWeatherServiceProxyAsync getInstance() {
            CurrentWeatherServiceProxyAsync
                    rs = GWT.create(CurrentWeatherServiceProxy.class);
            return rs;
        }
    }

    public CurrentWeather getWeatherByCity(String city);

    public String invokeGetRESTfulWebService(String uri) throws CurrentWeatherServiceException;
}