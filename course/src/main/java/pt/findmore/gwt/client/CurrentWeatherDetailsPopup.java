/**
 * $$Id$$
 * @author mribeiro
 * @date 25/02/15 19:58
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.client;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.Label;
import pt.findmore.gwt.shared.Utils;
import pt.findmore.gwt.shared.dto.CurrentWeather;

/**
 *
 */
public abstract class CurrentWeatherDetailsPopup extends DialogBox {

    private FlexTable content;
    private DockLayoutPanel root;
    private Button closeButton;
    private Image icon;

    public CurrentWeatherDetailsPopup() {
        super(true, true);
        initialize();
        addHandlers();
    }

    protected abstract void onClose();

    private void initialize() {
        root = new DockLayoutPanel(Style.Unit.PCT);
        content = new FlexTable();
        root.setSize("300px", "350px");
        content.addStyleName("formLayout");
        content.getColumnFormatter().setWidth(0, "100px");
        content.getColumnFormatter().setWidth(1, "150px");

        content.setWidget(0, 0, new FormLabel("Main weather"));
        content.setWidget(1, 0, new FormLabel("Description"));
        content.setWidget(2, 0, new FormLabel("Temperature"));
        content.setWidget(3, 0, new FormLabel("Humidity"));
        content.setWidget(4, 0, new FormLabel("Temp Min"));
        content.setWidget(5, 0, new FormLabel("Temp Max"));
        content.setWidget(6, 0, new FormLabel("Wind"));
        content.setWidget(7, 0, new FormLabel("Clouds"));
        content.setWidget(8, 0, new FormLabel("Rain"));
        content.setWidget(9, 0, new FormLabel("Snow"));

        closeButton = new Button("Close");
        HorizontalPanel buttons = new HorizontalPanel();
        buttons.setWidth("100%");
        buttons.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        buttons.add(closeButton);
        root.addSouth(buttons, 10);
        HorizontalPanel header = new HorizontalPanel();
        header.setWidth("100%");
        header.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        header.add(icon = new Image());
        root.addNorth(header, 20);
        root.add(content);
        super.add(root);
    }

    public void setContent(CurrentWeather currentWeather) {
        setText(currentWeather.getName());
        icon.setUrl(currentWeather.getWeather()[0].getIconUri());
        content.setWidget(0, 1, new FormValue(currentWeather.getWeather()[0].getMain()));
        content.setWidget(1, 1, new FormValue(currentWeather.getWeather()[0].getDescription()));
        content.setWidget(2, 1, new FormValue(Utils.formatTemperatureInCelsius(currentWeather.getMain().getTemp())));
        content.setWidget(3, 1, new FormValue(Utils.formatPercentage(currentWeather.getMain().getHumidity())));
        content.setWidget(4, 1, new FormValue(Utils.formatTemperatureInCelsius(currentWeather.getMain().getTemp_min())));
        content.setWidget(5, 1, new FormValue(Utils.formatTemperatureInCelsius(currentWeather.getMain().getTemp_max())));
        content.setWidget(6, 1, new FormValue(currentWeather.getWind().getDeg() + " deg - " + currentWeather.getWind().getSpeed() + " mps"));
        content.setWidget(7, 1, new FormValue(Utils.formatPercentage(currentWeather.getClouds().getAll())));
        content.setWidget(8, 1, new FormValue(currentWeather.getRain() != null ? currentWeather.getRain().get3h() + " mm" : "-"));
        content.setWidget(9, 1, new FormValue(currentWeather.getSnow() != null ? currentWeather.getSnow().get3h() + " mm" : "-"));
    }

    private void addHandlers() {
        closeButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                hide();
                onClose();
            }
        });
    }

    class FormLabel extends Label {
        public FormLabel(String text) {
            super(text + ":");
            addStyleName("label");
        }
    }

    class FormValue extends Label {

        public FormValue(String value) {
            super(value == null ? "<empty>" : value);
            addStyleName("fvalue");
        }
    }
}