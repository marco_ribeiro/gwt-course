/**
 * $$Id$$
 * @author mribeiro
 * @date 24/03/15 00:17
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.client.sweetalert;

import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.dom.client.StyleInjector;

/**
 *
 */
public enum SweetAlert {

    INSTANCE;

    SweetAlert()
    {
        initFramework();
    }

    public native void sweetError(String error)/*-{
        $wnd.swal({   title: "Error!",   text: "\""+error+"\"",   type: "error",   confirmButtonText: "Ok :(" });
    }-*/;

    private void initFramework(){
        ScriptInjector.fromString(FrameworkResources.INSTANCE.sweetAlertJs().getText()).inject();
        StyleInjector.inject(FrameworkResources.INSTANCE.sweetAlertCss().getText());

    }
}