package pt.findmore.gwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import pt.findmore.gwt.client.events.ErrorEvent;
import pt.findmore.gwt.client.service.CurrentWeatherServiceProxy;
import pt.findmore.gwt.client.service.CurrentWeatherServiceProxyAsync;
import pt.findmore.gwt.shared.FieldVerifier;
import pt.findmore.gwt.shared.dto.CurrentWeather;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Course implements EntryPoint {
    /**
     * The message displayed to the user when the server cannot be reached or
     * returns an error.
     */
    private static final String SERVER_ERROR = "An error occurred while "
            + "attempting to contact the server. Please check your network "
            + "connection and try again.";

    /**
     * Create a remote service proxy to talk to the server-side
     */
    private final CurrentWeatherServiceProxyAsync currentWeatherService = CurrentWeatherServiceProxy.Util.getInstance();

    private final Messages messages = GWT.create(Messages.class);

    private CurrentWeatherDetailsPopup popup;

    public static EventBus EVENT_BUS = GWT.create(SimpleEventBus.class);

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        final AppController appController = new AppController(EVENT_BUS, currentWeatherService);
        appController.init();
        DockLayoutPanel root = new DockLayoutPanel(Style.Unit.PCT);
        root.getElement().getStyle().setBackgroundColor("#3d3933");
        RootLayoutPanel.get().add(root);

        HTMLPanel title = new HTMLPanel("h1", "Current Weather Application");
        root.addNorth(title, 20);

        //footer logo
        HorizontalPanel footer = new HorizontalPanel();
        footer.setWidth("100%");
        footer.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        footer.add(new Image(AppImages.IMAGES.findmoreLogo()));
        root.addSouth(footer, 20);

        LayoutPanel content = new LayoutPanel();
        root.add(content);

        FlexTable form = new FlexTable();
        form.getElement().setAttribute("align", "center");

        content.add(form);

        Label label = new Label("Please enter a city name:");
        label.getElement().getStyle().setFontWeight(Style.FontWeight.BOLD);
        label.getElement().getStyle().setColor("white");
        form.setWidget(0, 0, label);
        form.getFlexCellFormatter().setColSpan(0,0,2);

        final TextBox nameField = new TextBox();
        nameField.setText(messages.nameField());
        form.setWidget(1, 0, nameField);

        final Button sendButton = new Button(messages.sendButton());
        sendButton.addStyleName("sendButton");
        form.setWidget(1, 1, sendButton);

        content.setSize("100%", "100%");

        FlexTable errorContent = new FlexTable();
        errorContent.getElement().setAttribute("align", "center");
        final Label errorLabel = new Label();
        errorLabel.getElement().getStyle().setColor("red");
        errorContent.setWidget(0,0,errorLabel);
        content.add(errorContent);
        content.setWidgetTopBottom(errorContent, 4, Style.Unit.EM, 0, Style.Unit.EM);

        // Focus the cursor on the name field when the app loads
        nameField.setFocus(true);
        nameField.selectAll();

        popup = new CurrentWeatherDetailsPopup() {
            @Override
            protected void onClose() {
                sendButton.setEnabled(true);
                nameField.setText(messages.nameField());
                nameField.setFocus(true);
                nameField.selectAll();
            }
        };


        // Create a handler for the sendButton and nameField
        class MyHandler implements ClickHandler, KeyUpHandler {
            /**
             * Fired when the user clicks on the sendButton.
             */
            public void onClick(ClickEvent event) {
                sendCityToServer();
            }

            /**
             * Fired when the user types in the nameField.
             */
            public void onKeyUp(KeyUpEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    sendCityToServer();
                }
            }

            /**
             * Send the name from the nameField to the server and wait for a response.
             */
            private void sendCityToServer() {
                // First, we validate the input.
                errorLabel.setText("");
                final String textToServer = nameField.getText();
                if (!FieldVerifier.isValidName(textToServer)) {
                    errorLabel.setText("Please enter at least four characters");
                    return;
                }

                // Then, we send the input to the server.
                sendButton.setEnabled(false);
                currentWeatherService.getWeatherByCity(textToServer, new AsyncCallback<CurrentWeather>() {
                    public void onFailure(Throwable caught) {
                        EVENT_BUS.fireEvent(new ErrorEvent("Error getting the current weather for "+textToServer));
                    }

                    public void onSuccess(CurrentWeather result) {
                        processCurrentWeather(result);
                    }
                });

            }
        }

        // Add a handler to send the name to the server
        MyHandler handler = new MyHandler();
        sendButton.addClickHandler(handler);
        nameField.addKeyUpHandler(handler);
    }

    private void processCurrentWeather(CurrentWeather weather){
        popup.setContent(weather);
        popup.center();
    }
}
