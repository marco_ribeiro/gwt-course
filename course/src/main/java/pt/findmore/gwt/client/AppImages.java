/**
 * $$Id$$
 * @author mribeiro
 * @date 06/03/15 21:24
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 *
 */
public interface AppImages extends ClientBundle {

    public static final AppImages IMAGES = GWT
            .create(AppImages.class);

    @ClientBundle.Source("images/findmoreLogo.png")
    ImageResource findmoreLogo();
}