/**
 * $$Id$$
 * @author mribeiro
 * @date 24/03/15 00:16
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.client.sweetalert;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

public interface FrameworkResources extends ClientBundle {
    /**
     * Static instance.
     */
    public FrameworkResources INSTANCE = GWT.create(FrameworkResources.class);

    /**
     * TextResource for sweet-alert.min.js.
     *
     * @return TextResource sweet-alert.min.js
     */
    @Source("../js/sweetalert/sweet-alert.min.js")
    TextResource sweetAlertJs();

    @Source("../js/sweetalert/sweet-alert.css")
    TextResource sweetAlertCss();

}
