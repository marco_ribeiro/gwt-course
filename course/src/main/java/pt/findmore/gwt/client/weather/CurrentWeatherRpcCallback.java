/**
 * $$Id$$
 * @author mribeiro
 * @date 22/02/15 17:30
 *
 * Copyright (C) 2015 BTC-ATM
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.client.weather;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 */
public abstract class CurrentWeatherRpcCallback implements AsyncCallback<String> {

    public void onFailure(Throwable caught) {
        String msg = caught.getMessage();
        if (msg != null)
            Window.alert(msg);
    }

    public void onSuccess(String result) {
        if (result == null)
            return;
        processResponse(result);
    }

    protected abstract void processResponse(String result);
}