/**
 * $$Id$$
 * @author mribeiro
 * @date 16/08/15 16:44
 *
 * Copyright (C) 2015 MRibeiro
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.client;

import com.google.gwt.event.shared.EventBus;
import pt.findmore.gwt.client.events.ErrorEvent;
import pt.findmore.gwt.client.service.CurrentWeatherServiceProxyAsync;
import pt.findmore.gwt.client.sweetalert.SweetAlert;

/**
 *
 */
public class AppController {

    private EventBus eventBus;
    private CurrentWeatherServiceProxyAsync service;


    public AppController(EventBus eventBus, CurrentWeatherServiceProxyAsync service) {
        this.eventBus = eventBus;
        this.service = service;
    }

    public void init() {
        initEventBusHandlers();
    }

    private void initEventBusHandlers() {
        this.eventBus.addHandler(ErrorEvent.TYPE, new ErrorEvent.ErrorEventEventHandler() {
            public void onError(ErrorEvent errorEvent) {
                SweetAlert.INSTANCE.sweetError(errorEvent.getError());
            }
        });
    }


}