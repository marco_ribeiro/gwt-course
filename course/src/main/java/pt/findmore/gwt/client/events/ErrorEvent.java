/**
 * $$Id$$
 * @author mribeiro
 * @date 16/08/15 16:46
 *
 * Copyright (C) 2015 MRibeiro
 *
 * All rights reserved.
 *
 */
package pt.findmore.gwt.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 *
 */
public class ErrorEvent extends GwtEvent<ErrorEvent.ErrorEventEventHandler> {

    public static GwtEvent.Type<ErrorEventEventHandler> TYPE = new Type<ErrorEventEventHandler>();
    private String error;

    public ErrorEvent(String error) {
        this.error = error;
    }

    @Override
    public Type<ErrorEventEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(ErrorEventEventHandler handler) {
        handler.onError(this);
    }

    public interface ErrorEventEventHandler extends EventHandler {
        void onError(ErrorEvent errorEvent);
    }

    public String getError() {
        return error;
    }
}